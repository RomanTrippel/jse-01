package ru.trippel.tm;

import ru.trippel.tm.controller.RequestVerification;
import ru.trippel.tm.util.Greeting;
import ru.trippel.tm.util.ReaderKeyboard;
import java.io.IOException;

public class Application {

    public static void main(String[] args) throws IOException {
        RequestVerification check = new RequestVerification();
        Greeting.greetingPrint();
        while (true) {
            check.add(ReaderKeyboard.read());
        }
    }

}
