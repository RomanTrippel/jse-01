package ru.trippel.tm.controller;

import ru.trippel.tm.entity.Reference;
import ru.trippel.tm.util.ReferencePrint;
import java.io.IOException;

public class RequestVerification {

    ProjectController projectController = new ProjectController();

    TaskController taskController = new TaskController();

    public void add(String request) throws IOException {
        if (Reference.HELP_MESSAGE.equals(request)) {
           ReferencePrint.referencePrintList();
        }
        else if ("project_create".equals(request)) {
            projectController.create();
        }
        else if ("project_view".equals(request)) {
            projectController.view();
        }
        else if ("project_edit".equals(request)) {
            projectController.edit();
        }
        else if ("project_delete".equals(request)) {
            projectController.delete();
        }
        else if ("task_create".equals(request)) {
            taskController.create();
        }
        else if ("task_view".equals(request)) {
            taskController.view();
        }
        else if ("task_edit".equals(request)) {
            taskController.edit();
        }
        else if ("task_delete".equals(request)) {
            taskController.delete();
        }
        else System.out.println("You entered an incorrect command, try again.");
    }

}
