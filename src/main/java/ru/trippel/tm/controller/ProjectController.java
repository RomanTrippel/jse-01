package ru.trippel.tm.controller;

import ru.trippel.tm.entity.Project;
import ru.trippel.tm.util.ReaderKeyboard;
import java.io.IOException;

public class ProjectController {

    public void create() throws IOException {
        System.out.println("Enter project name:");
        Project.getProjectList().add(new Project(ReaderKeyboard.read()));
        System.out.println("Project added");
    }

    public void view() {
        System.out.println("Project list:");
        for (int i = 0; i < Project.getProjectList().size(); i++) {
            System.out.println(i+1 + ". " + Project.getProjectList().get(i));
        }
    }

    public void edit() throws IOException {
        int number;
        String newName;
        view();
        System.out.println("Select a project to edit:");
        number = Integer.parseInt(ReaderKeyboard.read());
        System.out.println("Enter a new name:");
        newName = ReaderKeyboard.read();
        Project.getProjectList().get(number-1).setName(newName);
        System.out.println("Project name changed");
    }

    public void delete() throws IOException {
        int number;
        view();
        System.out.println("Select a project to delete:");
        number = Integer.parseInt(ReaderKeyboard.read());
        Project.getProjectList().remove(number-1);
        System.out.println("Project deleted");

    }

}
