package ru.trippel.tm.entity;

import java.util.ArrayList;

public class Project {

    private static ArrayList<Project> projectList = new ArrayList<Project>();

    private String name;

    public static ArrayList<Project> getProjectList() {
        return projectList;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Project(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }

}
