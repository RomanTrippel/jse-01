package ru.trippel.tm.entity;

import java.util.ArrayList;

public class Task {

    private static ArrayList<Task> taskList = new ArrayList<Task>();

    private String name;

    public static ArrayList<Task> getTaskList() {
        return taskList;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Task(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }

}
