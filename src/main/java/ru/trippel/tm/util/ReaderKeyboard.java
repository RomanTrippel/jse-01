package ru.trippel.tm.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ReaderKeyboard {

    static BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    public static String read() throws IOException {
        return reader.readLine();
    }

}
