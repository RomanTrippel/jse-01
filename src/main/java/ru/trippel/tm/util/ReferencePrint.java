package ru.trippel.tm.util;

import ru.trippel.tm.entity.Reference;

public class ReferencePrint {

    public static void referencePrintList() {
        String result = "";
        for (int i = 0; i < Reference.getReferenceList().size(); i++) {
            result += i+1 +". " + Reference.getReferenceList().get(i) + "\n";
        }
        System.out.println(result);
    }

}
